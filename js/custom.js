$(function () {
    if ($("#datepicker").length > 0) {
        $("#datepicker").datepicker({
            dateFormat: 'MM dd yy',
        });
        if ( document.documentElement.lang.toLowerCase() === "ru" ) {
        $.datepicker.setDefaults($.datepicker.regional['ru']);
        }else{
            $.datepicker.setDefaults($.datepicker.regional['en']);
        }
        // $("#timepicker").timepicker();
    }

});

mobiscroll.setOptions({
    theme: 'ios',
    themeVariant: 'light'
});

$(function () {
    $('#demo-time').mobiscroll().datepicker({
        controls: ['time'],
        touchUi: true,
        display: 'anchored',
        stepMinute: 15,
        circular: [true, true, false]

    });

    $('#demo-12h').mobiscroll().datepicker({
        controls: ['time'],
        timeFormat: 'HH:mm'
    });

    $('#demo-24h').mobiscroll().datepicker({
        controls: ['time'],
        timeFormat: 'h:mm A'
    });

    $('#demo-hms').mobiscroll().datepicker({
        controls: ['time'],
        timeFormat: 'HH:mm:ss',
        headerText: 'Time: {value}'
    });
});



$(document).ready(function () {
    // $("#datepicker").change(function(){
    //     $("#datepicker").val($.datepicker.formatDate('dd M yy', new Date()));
    // });
    var numberPattern = /\d+/g;


    $("input.number").keyup(function () {
        $(this).val($(this).val().match(numberPattern));
    });

    //var currentValue = parseInt($("#passengers").val());



    $('.up').click(function () {
        if (!$(this).hasClass("readonly")) {
            let currentValue = parseInt($(this).parent().children("input").val());
            currentValue++
            $(this).parent().children("input").val(currentValue);
        }
    });
    $('.down').click(function () {
        if (!$(this).hasClass("readonly")) {


            let currentValue = parseInt($(this).parent().children("input").val());
            if (currentValue > 0) {
                let currentValue = parseInt($(this).parent().children("input").val());
                currentValue--
                $(this).parent().children("input").val(currentValue);
            }
        }
    });


    $('#children-options > .children-number-text, #children').click(function () {
        if($(this).hasClass("ok")){
            $('#children-options > .children-number-text, #children').removeClass("ok");
            $(".children-number-text-left").text("Set (");
        }
        $(".children-options-container").toggle();
        
    });
    

    $('.children-options-container .form-input3 > .up, .children-options-container .form-input3 > .down').click(function () {
        var sum = 0;
        $(".children-options-container .form-input3 input").each(function (index) {
            sum += parseFloat($(this).val());
        });
        $("#children").val(sum);
        $("#children").change();
    });

    $('#children').on("change", function () {
        console.log("changed")
        $(".children-number-text-left").text("OK (");
        $('#children-options > .children-number-text, #children').addClass("ok")
    });

    $("#change_loc_dest_val").click(function () {
        console.log("aee", $("#destination").val());
        if ($("#location").val() !== "" && $("#destination").val() !== "") {
            console.log("shevida");
            var validateSelect1 = false;
            var validateSelect2 = false;
            $('#location option').each(function () {
                if (this.value === $("#destination").val()) {
                    validateSelect1 = true;
                }
            });
            $('#destination option').each(function () {
                if (this.value === $("#destination").val()) {
                    validateSelect2 = true;
                }
            });
            if (validateSelect2 && validateSelect1) {
                var destination = $("#destination").val();
                $("#destination").val($("#location").val());
                $("#location").val(destination);
            }
        }
    });

    $(".show-mobile-menu").click(function () {
        $("nav.navigation").addClass("show");
    });
    $(".hide-mobile-menu").click(function () {
        $("nav.navigation").removeClass("show");
    });


    if ($(".payment-card-form-number-input").length > 0) {

        $(".payment-card-form-number-input").keyup(function () {
            var card_number = "";
            $(".payment-card-form-number-input").each(function () {
                card_number += $(this).val()

            })
            $(this).val($(this).val().match(numberPattern));

            if ($(this).val().length === 4) {
                if ($(this).next("input").length > 0) {
                    $(this).next("input").focus();
                }
            }

            $("#card_number").val(parseInt(card_number));
        })

    }

    if ($(".payment-card-form-date-input").length > 0) {
        $(".payment-card-form-date-input").keyup(function () {
            $(this).val($(this).val().match(numberPattern));

            if ($(this).val().length === 2) {
                if ($(this).next("input").length > 0) {
                    console.log("focus");
                    $(this).next("input").focus();
                }
            }
        });
    }
    $(".payment-button").click(function (e) {
        console.log("clicked");
        var validate = $(".payment-input").length;
        $(".payment-input").each(function () {
            if ($(this).val() !== "") {
                validate--;
            }
        })
        if (validate === 0) {
            e.preventDefault();

            $(".payment-success-popup").show();
        }
    });

    $(".close-popup").click(function () {
        $(".payment-success-popup").hide();
    });

    $(".form-slides .form-submit button").click(function (event) {
        event.preventDefault();
        
        var validate2 = $(".form-slides form input.required, .form-slides select").length;
        console.log("validate2", validate2)

        $(".form-slides form input.required, .form-slides form select").each(function (i) {
            console.log($(this).val(), " index:" + i);
            if ($(this).val() !== "") {
                $(this).parent().removeClass("error");
                validate2--;
            } else {
                $(this).parent().addClass("error");
                validate2++;
            }
        })
        // if($(".error").length > 0){
        //     $("html").animate({
        //         scrollTop: $(".error").offset().top
        //     }, 500);
        // }

        

        if (validate2 === 0) {
            $(".form-slides form").submit();
            // $(".form.transfer").hide('slide', { direction: 'left' }, 1000);
            // $(".form.passenger").show('slide', { direction: 'right' }, 1000);
        }
    });
});